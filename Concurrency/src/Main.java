import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;


public class Main {
	
	
	
	public static void main(String[] args) {
		List<String> buffer = new ArrayList<String>();
		
		ReentrantLock lock = new ReentrantLock();
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		
		MyProducer producer = new MyProducer(buffer, lock);
		MyConsumer consumer1 = new MyConsumer(buffer,lock);
		MyConsumer consumer2 = new MyConsumer(buffer,lock);
		
		executorService.execute(producer);
		executorService.execute(consumer1);
		executorService.execute(consumer2);
		
		Future<String> future = executorService.submit(
			new Callable<String>() 
			{				
				public String call() throws Exception{
					System.out.println("I'm Printed from callable ");
					return "This is the callable result";
				}
			}
		);
		
		try {			
				System.out.println(future.get());
		} catch(ExecutionException e) {
				System.out.println("Something went wrong");
		} catch(InterruptedException e) {
				System.out.println("Thread running the task was interrupted");
		}
		
		executorService.shutdown();
	}
}
	
	class MyProducer implements Runnable{
		private List<String> buffer;
		private ReentrantLock lock;
			
		public MyProducer(List<String> buffer, ReentrantLock lock) {
			super();
			this.buffer = buffer;
			this.lock = lock;
		}

		@Override
		public void run() {
			String nums[] = {"1","2","3","4","5"};
			for(String num : nums) {
				try {
					System.out.println("Adding "+ num);
 					lock.lock(); // checks for lock available and gives lock if it is otherwise keep blocking thread 
					try {
						buffer.add(num);
					}finally {
						lock.unlock(); // unlocks the buffer  and made lock available to other threads
					}
					Thread.sleep(1000);
						
				}catch(InterruptedException e) {
					System.out.println("Producer was interrupted");
				}
			}
			
			System.out.println("EOF and closing");
			
			if(lock.tryLock()) {
				try {
					buffer.add("EOF");
				}finally {
					lock.unlock();
				}
			}
			
		}
		
		
	}
	
	class MyConsumer implements Runnable{

		private List<String> buffer;
		private ReentrantLock lock;
		
		public MyConsumer(List<String> buffer, ReentrantLock lock) {
			this.buffer = buffer;
			this.lock = lock;
		}

		@Override
		public void run() {
			
			while(true) {
				if(lock.tryLock()) {
					try {
						if(buffer.isEmpty()) {
							continue;
						}
						
						if(buffer.get(0).equals("EOF")) {
							System.out.println("Exiting");
							break;
						}else {
							System.out.println("Removing "+buffer.get(0));
							buffer.remove(0);
						}
					}finally {
						lock.unlock();
					}
				}												
			}
			
		}
		
	}




















