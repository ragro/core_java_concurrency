package ArrayBlockingQueueCode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;


public class Main {
	
	
	
	public static void main(String[] args) {
		ArrayBlockingQueue<String> buffer = new ArrayBlockingQueue<String>(6);
		
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		
		MyProducer producer = new MyProducer(buffer);
		MyConsumer consumer1 = new MyConsumer(buffer);
		MyConsumer consumer2 = new MyConsumer(buffer);
		
		executorService.execute(producer);
		executorService.execute(consumer1);
		executorService.execute(consumer2);
		
		Future<String> future = executorService.submit(
			new Callable<String>() 
			{				
				public String call() throws Exception{
					System.out.println("I'm Printed from callable ");
					return "This is the callable result";
				}
			}
		);
		
		try {			
				System.out.println(future.get());
		} catch(ExecutionException e) {
				System.out.println("Something went wrong");
		} catch(InterruptedException e) {
				System.out.println("Thread running the task was interrupted");
		}
		
		executorService.shutdown();
	}
}
	
	class MyProducer implements Runnable{
		private ArrayBlockingQueue<String> buffer;
			
		public MyProducer(ArrayBlockingQueue<String> buffer) {
			super();
			this.buffer = buffer;
		}

		@Override
		public void run() {
			String nums[] = {"1","2","3","4","5"};
			for(String num : nums) {
				try {
					System.out.println("Adding "+ num);
					buffer.put(num);
					Thread.sleep(1000);
						
				}catch(InterruptedException e) {
					System.out.println("Producer was interrupted");
				}
			}
			
			System.out.println("EOF and closing");
				
				try {
					buffer.put("EOF");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}
		
	
	class MyConsumer implements Runnable{

		private ArrayBlockingQueue<String> buffer;
		
		public MyConsumer(ArrayBlockingQueue<String> buffer) {
			this.buffer = buffer;
		}

		@Override
		public void run() {
			
			while(true) {
				synchronized(buffer) {
					try {
						if(buffer.isEmpty()) {
							continue;
						}
						
						if(buffer.peek().equals("EOF")) {
							System.out.println("Exiting");
							break;
						}else {
							System.out.println("Removing "+buffer.peek());
							buffer.take();
						}
					}catch(InterruptedException e) {
						System.out.println("Thread is interrupted in cunsumer side");
					}
				}
					
			}												
		}
			
	}
		
	




















