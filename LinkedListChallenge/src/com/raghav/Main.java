package com.raghav;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

/**
 * @author rohit
 *
 */
public class Main {
	
	private static ArrayList<Album> albums = new ArrayList<Album>();
	
	public static void main(String[] args) {
		Album album = new Album("ArjitSpecial","Arjit Singh");
		album.addSong("hasi ban gye", 3.44);
		album.addSong("chahu main ya na", 4.55);
		album.addSong("Channa Mereya", 5.09);
		album.addSong("Sun rha h tu", 3.33);
		album.addSong("Raabta", 2.45);
		
		albums.add(album);
		
		album = new Album("AtifSpecial","Atif Aslam");
		album.addSong("Be inthaah",4.58 );
		album.addSong("Kuch is trh",3.28 );
		album.addSong("Dil Diyan gallan",2.58 );
		album.addSong("Jeene laga huh",4.38 );
		album.addSong("Tu Jaane na",3.48 );
		
		albums.add(album);
		
		LinkedList<Song>playlist = new LinkedList<Song>();
		albums.get(0).addToPlayList("Sun rha h tu", playlist);
		albums.get(0).addToPlayList(1, playlist);
		albums.get(0).addToPlayList(3, playlist);
		albums.get(0).addToPlayList("Raabta", playlist);
		albums.get(1).addToPlayList("Tu Jaane na", playlist);
		albums.get(1).addToPlayList(2, playlist);
		albums.get(1).addToPlayList("Kuch is trh", playlist);
		albums.get(1).addToPlayList("Tum mere ho", playlist);
		albums.get(0).addToPlayList(9, playlist);
		
		play(playlist);
		
	}
	
	private static void play(LinkedList<Song>playList) {
		
		Scanner scanner = new Scanner(System.in);
		ListIterator<Song> listiterator = playList.listIterator();
		
		boolean quit = false;
		boolean forward = true;
		
		if(playList.size()== 0){
			System.out.println("\nNo Song available in playlist");
		}else {
			System.out.println("Now Playing "+ listiterator.next().toString());
		}
		
		printMenu();
		
		while(!quit) {
				
			int action = scanner.nextInt();
			
			scanner.nextLine();
			
			switch(action) {
			
				case 1: 
					System.out.println("\n22Playlist is stopped playing");
					quit = true;
					break;
					
				case 2: 
					if(!forward) {
						forward = true;	
						if(listiterator.hasNext()) {
							listiterator.next();
						}
					}					
					if(listiterator.hasNext()) {
						System.out.println("Now playing "+ listiterator.next().toString());
					}else {
						System.out.println("We reached end of the list");
						forward = false;
					}
					break;
					
				case 3: 
					if(forward) {
						if(listiterator.hasPrevious()) {
							listiterator.previous();
						}
						forward = false;						
					}
					if(listiterator.hasPrevious()) {
						System.out.println("Now Playing "+ listiterator.previous());
					}else {
						System.out.println("We are in starting of playlist");
					}
					break;
	
				case 4: 
					printList(playList);
					break;
				
				case 5: 
					if(forward) {
						if(listiterator.hasPrevious()) {
							System.out.println("Now playing :" + listiterator.previous().toString());
							forward = false;
						}else {
							System.out.println("We are at start of playlist");
						}
						
					}else {
						if(listiterator.hasNext()) {
							System.out.println("Now Playing :" + listiterator.next().toString());
							forward = true;
						}else {
							System.out.println("We are at end of playlist");
						}
						
					}
					break;
				
				case 6:
					printMenu();
					break;
					
				case 7:
					if(playList.size() > 0) {
						listiterator.remove();
						if(listiterator.hasNext()) {
							System.out.println("Now Playing "+ listiterator.next().toString());
						}else if(listiterator.hasPrevious()) {
							System.out.println("Now Playing "+ listiterator.previous().toString());
						}
					}else {
						System.out.println("Playlist is empty");
					}
					break;
			}
		}
		
	}
	
	private static void printMenu() {
		System.out.println("==========Menu===========");
		System.out.println("Press 1 : to quit\n"+
				"Press 2 : to play forward\n"+
				"Press 3 : to play backward\n"+
				"Press 4 : to print the playlist\n"+
				"Press 5 : to replay\n"+
				"Press 6 : to Print Menu\n"+
				"Press 7 : to delete current song from playlist");
	}
	
	private static void printList(LinkedList<Song>playList) {
		ListIterator<Song> list = playList.listIterator();
		System.out.println("==========================");
		while(list.hasNext()) {
			System.out.println(list.next().toString());
		}
		System.out.println("==========================");
	}

}
