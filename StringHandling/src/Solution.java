import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static int minimumNumber(int n, String password) {
        // Return the minimum number of characters to make the password strong
        int minNumber=0;
        
        boolean flagDigit= false,flagLower= false,flagUpper= false,flagSpecial =false;
        String special_characters = "!@#$%^&*()-+";
        String numbers = "0123456789";
        
        
        for(int i=0; i<password.length(); i++){
            //for digit    
            if(!flagDigit){
                    for(int j=0; j<numbers.length(); j++){
                        if( ((int)password.charAt(i) == (int)numbers.charAt(j)) && (!flagDigit) ){
                            flagDigit = true;                            
                        }
                    }
                }
            //for lower case
            if(!flagLower){
                if( ( (int)password.charAt(i)>=97 ) && ( (int)password.charAt(i) <=122) ){
                    flagLower = true;
                }
            }
            
            //forUppercase
            if(!flagUpper){
                if( ( (int)password.charAt(i)>=65 ) && ( (int)password.charAt(i) <=90) ){
                    flagUpper = true;
                }
            }
            
            //forLowercase
            if(!flagSpecial){
                for(int j=0; j<special_characters.length(); j++){
                        if( password.charAt(i) == special_characters.charAt(j) ){
                            flagSpecial = true;
                        }
                    }
            }
        }
        
        if(!flagDigit){
            minNumber++;
        }
        if(!flagLower){
            minNumber++;
        }
        if(!flagUpper){
            minNumber++;
        }
        if(!flagSpecial){
            minNumber++;
        }
        
        
        while( (password.length() + minNumber) <6){
        	System.out.println("character "+c);
            minNumber++;
        }
        return minNumber;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String password = in.next();
        int answer = minimumNumber(n, password);
        System.out.println(answer);
        in.close();
    }
}
