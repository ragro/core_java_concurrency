package com.io.file.collections;

import java.util.Random;
import java.util.Set;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.LinkedHashSet;
import java.util.Iterator;

public class DifferentSet {

	public static void main(String[] args) {

		Random random = new Random();
		
		HashSet<Integer> hashSet = new HashSet<>();
		Set<Integer> linkedHashSet = new LinkedHashSet<>();
		Set<Integer> treeSet = new TreeSet<>();
		
		for(int i=0; i<8;i++) {
			int j = random.nextInt(50);
			hashSet.add(j);
			System.out.print(" "+j);
		}
		System.out.println("\nHashSet : "+hashSet);
		
		for(int i=0; i<8;i++) {
			int j = random.nextInt(50);
			linkedHashSet.add(j);
			System.out.print(" "+j);
		}
		System.out.println("\nLinkedHashSet :"+linkedHashSet);
		
		for(int i=0; i<8;i++) {
			int j = random.nextInt(50);
			treeSet.add(j);
			System.out.print(" "+j);
		}
				
		System.out.println("\nTreeSet :"+treeSet);
		
		Iterator itr = hashSet.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
			itr.remove();
		}
		
		System.out.println(hashSet);
	}

}
