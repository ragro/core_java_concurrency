package com.io.file.collections;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetStringDemo {

	public static void main(String[] args) {
		Set<String> set = new TreeSet<>();
		
		set.add("abc");
		set.add("bcd");
		set.add("xyz");
		set.add("fgh");
		
		for(String s : set) {
			System.out.println(s);
		}
			
	}

}
//Note : for adding a class say String, Integer or StringBuffer into a TreeSet, the class should 
// provide implementation of comparator or comparable. StringBuffer can't be added bcz it does not provide 
//implementation of comparator or comparable.. to do so we need to make custom comparator
