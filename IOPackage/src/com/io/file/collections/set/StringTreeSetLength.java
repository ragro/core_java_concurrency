package com.io.file.collections.set;

import java.util.Set;
import java.util.TreeSet;

import com.io.file.collections.comparator.StringLengthComparator;

public class StringTreeSetLength {
	public static void main(String[] args) {
		Set<String> treeSet = new TreeSet<>(new StringLengthComparator());
		
		treeSet.add("mno");
		treeSet.add("abcdg");
		treeSet.add("xyzwer");
		treeSet.add("defzzzzzzzzzz");
		
		for (String string : treeSet) {
			System.out.println(string);
		}
//		System.out.println(treeSet);
	}
}
