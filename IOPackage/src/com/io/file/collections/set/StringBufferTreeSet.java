package com.io.file.collections.set;

import java.util.Set;
import java.util.TreeSet;

import com.io.file.collections.comparator.SBComparator;

public class StringBufferTreeSet {

	public static void main(String[] args) {
		@SuppressWarnings("unchecked")
		Set<StringBuffer> set = new TreeSet<>(new SBComparator());
		
		set.add(new StringBuffer("abc"));
		set.add(new StringBuffer("def"));
		set.add(new StringBuffer("rgh"));
		
		for(StringBuffer s : set) {
			System.out.println(s);
		}
	}

}

//Comparable interface used to define natural sorting order of class objects and comparator for define 
// custom sorting order
