package com.io.file.collections.list;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class ListBackward {

	public static void main(String[] args) {
		List<String> list = new LinkedList<>();
		
		list.add("akash");
		list.add("adb");
		list.add("raghu");
		
		ListIterator<String> litr = list.listIterator();

		System.out.println("\nReading list from forward side : \n");
		
		while(litr.hasNext()) {
			System.out.println(litr.next());
		}
		
		System.out.println("\nReading list from backward side : \n");
		
		while(litr.hasPrevious()) {
			System.out.println(litr.previous());
		}
	}

}
