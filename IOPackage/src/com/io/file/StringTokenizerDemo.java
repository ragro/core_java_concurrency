package com.io.file;

import  java.util.StringTokenizer;
public class StringTokenizerDemo {

	public static void main(String[] args) {
		String st = "Serialization and/deserialization occur/automatically by.java runtime system, Garbage collection also occur automatically but is done by CPU or the operating system not by the java runtime system.";
		
		StringTokenizer tokenizer = new StringTokenizer(st, " /.");
		int count = 0;
		while(tokenizer.hasMoreTokens()) {
			System.out.println(tokenizer.nextToken());
			count++;
		}
		
		System.out.println("Total words : "+count);
 	}

}
