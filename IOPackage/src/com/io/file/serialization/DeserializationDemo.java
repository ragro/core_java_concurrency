package com.io.file.serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationDemo {

	public static void main(String[] args) {
		File file = new File("/home/rohit/eclipse-workspace/IOPackage/src/com/io/file/serialization/Emp.ser");
		try {
			FileInputStream fin = new FileInputStream(file);
			ObjectInputStream obj = new ObjectInputStream(fin);
			
			Object empObj = obj.readObject();
			Employee emp = (Employee)empObj;
			emp.setCity("Faridabad");
			
			System.out.println("Employee Id : "+emp.empId);
			System.out.println("Employee Name : "+emp.empName);
			System.out.println("Employee Salary : "+emp.salary);
			System.out.println("Employee City : "+emp.getCity());
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		
	}

}
