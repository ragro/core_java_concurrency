package com.io.file.serialization;

import java.io.Serializable;

public class Employee implements Serializable {
	int empId;
	String empName;
	int salary;
	transient String  city;
	
	public Employee(int empId, String empName, int salary, String city) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.salary = salary;
		this.city = city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCity() {
		return this.city;
	}
	
	
}
