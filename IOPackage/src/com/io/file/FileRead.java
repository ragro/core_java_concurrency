package com.io.file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileRead {
	public static void main(String[] args) {
		//give name of file
		String fileName = "/home/rohit/eclipse-workspace/IOPackage/src/file.txt";
		//take a string as reference for each line
		String line = null;
		//wrap file in to fileReader (for txt) and FileInputStream for binary one
				try {
					FileReader file = new FileReader(fileName);
					//wrap fileReader in to BufferedReader
					
					BufferedReader bufferedReader = new BufferedReader(file);
					//read file line by line
					
					while( (line = bufferedReader.readLine()) != null ) {
						System.out.println(line);
					}
					//close the bufferReader
					bufferedReader.close();
				}
				catch(FileNotFoundException error) {
					System.out.println("Unable to find file named: "+fileName);
				}
				catch(IOException err) {
					System.out.println("Unable to read file : "+fileName);
				}
	}
}
