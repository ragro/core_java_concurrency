package com.io.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamDemo {

	public static void main(String[] args) {
		FileInputStream fis = null;
		FileOutputStream fout = null;
		
		File file = new File("/home/rohit/eclipse-workspace/IOPackage/2017-03-15-12-01-57-223.jpg");
		File newFile = new File("/home/rohit/eclipse-workspace/IOPackage/college.jpg");

		
		try {
			fis = new FileInputStream(file);
			fout = new FileOutputStream(newFile);
			
			int i;
			while((i= fis.read()) != -1) {
				fout.write(i);
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				fis.close();
				fout.close();
			}catch(IOException e) {
				e.printStackTrace();
			}
			
		}
	}

}
