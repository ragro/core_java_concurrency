package com.io.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadWrite {

	public static void main(String[] args) {

		//give file name
		String fileName="/home/rohit/eclipse-workspace/IOPackage/src/file.txt";
		String newfileName="/home/rohit/eclipse-workspace/IOPackage/src/newFile.txt";

		//get reference to hold line from file
		String line = null;
		
		try {
			//pass file to filereader
			FileReader file = new FileReader(fileName);
			BufferedReader reader = new BufferedReader(file);
			
			FileWriter newFile = new FileWriter(newfileName);
			BufferedWriter writer = new BufferedWriter(newFile);
			
			while( (line = reader.readLine()) != null) {
				writer.write(line);
				writer.newLine();
			}
			
			writer.close();
			reader.close();
			
		}catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}
		
		
		//wrap filereader  in bufferedReader
		
		//close buffered Reader
	}

}
