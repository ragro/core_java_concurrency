package Challenge1_2;

public class AccountHolder {

	public static void main(String[] args) {

	    BankAccount account  = new BankAccount(1000,"123456789");
		Thread th1 = new Thread(new Runnable() {
			public void run() {
				account.deposit(500);
				account.withdraw(300);
			}
		});
		
		Thread th2 = new Thread(new Runnable() {
			public void run() {
				account.deposit(50);
				account.withdraw(250);
			}
		});
		
		th1.start();
		th2.start();
	}	
}
