package Challenge1_2;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {
	
	@SuppressWarnings("unused")
	private double balance;

	@SuppressWarnings("unused")
	private String accountNumber;
	
	private Lock lock; 

	public BankAccount(double balance, String accountNumber) {
		this.balance = balance;
		this.accountNumber = accountNumber;
		this.lock = new ReentrantLock();
	}
	
	 public void deposit(double amount) {
			
		 try {
			 	if(lock.tryLock(1000, TimeUnit.MILLISECONDS)) {
			 		try {
			 				balance += amount;
			 		}finally {
			 			lock.unlock();
			 		}
			 	}else {
			 		System.out.println("Can't get lock...");
			 	}
		 }catch(InterruptedException e) {
			 System.out.println(e.getMessage());
		 }
//		 lock.lock();
//		 	try{
//				balance += amount;
//			}finally {
//				lock.unlock();
//			}
	}
	
	 public void withdraw(double amount) {
		 
		 try {
			 if(lock.tryLock(1000, TimeUnit.MILLISECONDS)) {
				 try {
					  balance -= amount;
				 }finally {
					 lock.unlock();
				 }
			 }else {
				 System.out.println("Cant get lock...");
			 }
		 }catch(InterruptedException e) {
			 System.out.println(e.getMessage()); 
		 }
//		
//		 lock.lock();
//		 try{
//				balance += amount;
//			}finally {
//				lock.unlock();
//			}
	}
	 
	 public String getAccountNumber() {
		 return accountNumber;
	 }
	 
	 public void printaccountNumber() {
		 System.out.println("AccountNumber :- "+accountNumber);
	 }
}
